


import java.io.Console;
import java.util.Scanner;
import java.util.concurrent.*;



public class BALbis {	
	
	private BlockingQueue <String> queue = new ArrayBlockingQueue<String>(2);
	

	public String read () throws InterruptedException{
			return queue.take();
	}
	
	public boolean write (String lettre) throws InterruptedException{
		try{
			queue.put(lettre);
			return true;
		}catch(InterruptedException e){
			return false;
		}
	}
	

}
