
import java.io.Console;
import java.util.Scanner;


public class BAL {
	
	String []lettre = {"A","B","C","D","*"};
	String []buffer = new String [lettre.length];
	int available = 0;
	int queue = 0;
	int tete = 0;
	public synchronized void read (){
		
		if(!lettre[queue].equals("*")){
			while (available <=0){
				try{
					wait(1000);
				}catch (InterruptedException e){}
			}
			System.out.println("Je lis : " + lettre[queue]);
			queue ++;
			available --;
			notifyAll();
		}
	}
	
	public synchronized void write (){
		
		if(!lettre[tete].equals("*")){
			
			while (available > lettre.length){
				try{
					wait(100);
				}catch (InterruptedException e){}
			}
			buffer[tete] = lettre[tete];
			System.out.println("J'ai écris : " + lettre[tete]);
			tete ++;
			available ++;
			notifyAll();
		}
	}
	

}
