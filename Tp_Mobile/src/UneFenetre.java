import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

class UneFenetre extends JFrame implements ActionListener 
{
    UnMobile [] sonMobile;
    private final int LARG=900, HAUT=800;
    JButton [] boutons ;
    Thread [] threads;
    Boolean [] etats;
    GridLayout layout ;
    semaphoreGen sem;
    
    
    public UneFenetre(int nb, int res)
    {
	    super("Le Mobile"); 
	    
	    sonMobile = new UnMobile[nb];
	    boutons = new JButton[nb];
	    threads = new Thread[nb];
	    etats = new Boolean[nb];
	    
	    GridLayout layout = new GridLayout(nb,2);
	    
	    sem = new semaphoreGen(res);
	    
		// ajouter sonMobile a la fenetre
	    Container leConteneur = getContentPane();
	    leConteneur.setLayout(layout);
	
	
	    
	    for(int i = 0; i < boutons.length; i++){
	    	etats[i] = true;
	        sonMobile[i] = new UnMobile(LARG/2,HAUT/nb,sem);
	        
	    	boutons[i]=new JButton("Start/Stop");
	    	boutons[i].addActionListener(this);
	    
	    	leConteneur.add(sonMobile[i]);
	    	leConteneur.add(boutons[i]);
	    	
	    	threads[i] = new Thread(sonMobile[i]);
	        threads[i].start();
	    }
	    
	    
		// afficher la fenetre
	    setSize(LARG, HAUT);
	    setVisible(true);
    }

	@Override
	public void actionPerformed(ActionEvent e) {
		for(int i = 0; i < boutons.length; i++){
			if(e.getSource()==boutons[i] && etats[i] == true){
				threads[i].suspend();
				etats[i] = false;
			}
			else if (e.getSource()==boutons[i] && etats[i] == false){
				threads[i].resume();
				etats[i] = true;
				
			}
		}
	}
	
}