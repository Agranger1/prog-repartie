import java.awt.*;
import javax.swing.*;

class UnMobile extends JPanel implements Runnable
{
    int saLargeur, saHauteur, sonDebDessin;
    final int sonPas = 10, sonTemps=50, sonCote=40;
    semaphoreGen sem;
    
    UnMobile(int telleLargeur, int telleHauteur, semaphoreGen parSem)
    {
	super();
	saLargeur = telleLargeur;
	saHauteur = telleHauteur;
	setSize(telleLargeur, telleHauteur);
	sem = parSem;
    }

    public void run()
    {
    	
		for (sonDebDessin=0; sonDebDessin < saLargeur - 4*sonPas; sonDebDessin+= sonPas)
		    {
			
			if(sonDebDessin < 150){
				System.out.println("ah");
				sem.syncWait();
			}
			
			try{Thread.sleep(sonTemps);}
			catch (InterruptedException telleExcp)
			    {telleExcp.printStackTrace();}
		    }
		sem.syncSignal();
		
		repaint();
		for (; sonDebDessin > 0 -2*sonPas; sonDebDessin-= sonPas)
	    	{
			
			if(sonDebDessin < 150)
				sem.syncSignal();
			if(sonDebDessin > 250)
				sem.syncWait();
			repaint();
			try{Thread.sleep(sonTemps);}
			catch (InterruptedException telleExcp)
		    {telleExcp.printStackTrace();}
	    	}
	    
    }

    public void paintComponent(Graphics telCG)
    {
	super.paintComponent(telCG);
	telCG.fillRect(sonDebDessin, saHauteur/2, sonCote, sonCote);
    }
}